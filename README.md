# Flake VTE Notifications
An overlay that overrides the following applications:
* gnome-terminal
* tilix

to be built with a VTE terminal widget with notification support.

# Installation
This currently only supports the experimental Nix Flakes interface for NixOS (nixUnstable).
```nix
# This is an example flake.nix for a NixOS system

{
  inputs.flake-vte-notifications = {
    url = "https://codeberg.org/worldofpeace/flake-vte-notifications";
    type = "git";
    ref = "main";
    # This flake should support stable and nixos-unstable
    # so feel free to make it follow your system's nixpkgs
    # flake.
    inputs.nixpkgs.follows = "nixpkgs"; # or what the nixpkgs flake is called in your configuration
  };

  outputs = { self, nixpkgs, flake-vte-notifications }: {
     # replace 'joes-desktop' with your hostname here.
     nixosConfigurations.joes-desktop = nixpkgs.lib.nixosSystem {
       system = "x86_64-linux";
       modules = [ ./configuration.nix flake-vte-notifications.nixosModule ];
     };
  };
}
```

and install either of the aforementioned applications in their supported configuration in NixOS.
