{
  description = "Terminal apps built with patches to vte for providing notifications.";

  outputs = { self, nixpkgs }: {
    nixosModule = import ./modules { inherit (self) overlay; };
    overlay = import ./overlay;

    nixosConfigurations.example-system = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [
        self.nixosModule
        ./test/configuration.nix
      ];
    };

    packages = let
      pkgs = import nixpkgs {
        system = "x86_64-linux";
        config = {};
        overlays = [ (import ./overlay) ];
      };
    in {
      inherit (pkgs) tilix;
      inherit (pkgs.gnome3) gnome-terminal;
    };

    checks."x86_64-linux"."example-system" = self.nixosConfigurations.example-system.config.system.build.toplevel;
  };
}
