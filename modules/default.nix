{ overlay }:

{ config, lib, pkgs, ... }:

{
  imports = [
    ./vte-notifications.nix
  ];

  nixpkgs.overlays = [
    overlay
  ];

  vte-notifications = {
    environment.vte.package = pkgs.vte-notifications;
    programs.bash.vteIntegration = true;
    programs.zsh.vteIntegration = true;
  };
}
