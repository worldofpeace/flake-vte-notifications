# VTE

{ config, pkgs, lib, ... }:

with lib;

let

  cfg = config.vte-notifications;

  vteInitSnippet = ''
    # Show current working directory in VTE terminals window title.
    # Supports both bash and zsh, requires interactive shell.
    . ${cfg.environment.vte.package}/etc/profile.d/vte.sh
  '';

in

{

  # Don't collide with the module in nixpkgs
  options.vte-notifications = {

    programs.bash.vteIntegration = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Whether to enable Bash integration for VTE terminals.
        This allows it to preserve the current directory of the shell
        across terminals.
      '';
    };

    programs.zsh.vteIntegration = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Whether to enable Zsh integration for VTE terminals.
        This allows it to preserve the current directory of the shell
        across terminals.
      '';
    };

    environment.vte.package = mkOption {
      type = types.package;
      default = pkgs.vte;
      description = "Which vte package to use.";
    };

  };

  config = mkMerge [
    (mkIf cfg.programs.bash.vteIntegration {
      programs.bash.interactiveShellInit = mkBefore vteInitSnippet;
    })

    (mkIf cfg.programs.zsh.vteIntegration {
      programs.zsh.interactiveShellInit = vteInitSnippet;
    })
  ];
}
