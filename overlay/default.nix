final: prev:

# Add Notifcations support to Tilix
# (gnome-terminal is possible as well)
let
  inherit (final)
    lib
    fetchpatch
  ;

  gnome-terminal-majorminor-version = lib.versions.majorMinor (lib.getVersion final.gnome3.gnome-terminal);
  mkGnomeTerminalPatchUrl = commit: "https://src.fedoraproject.org/rpms/gnome-terminal/raw/${commit}/f/gnome-terminal-cntr-ntfy-autottl-ts.patch";

  vte-majorminor-version = lib.versions.majorMinor (lib.getVersion final.vte);
  mkVtePatchUrl = commit: "https://src.fedoraproject.org/rpms/vte291/raw/${commit}/f/vte291-cntnr-precmd-preexec-scroll.patch";
in
{
  # Use fedora's patch for notification support in the vte widget
  vte-notifications = prev.vte.overrideAttrs (old: {
    patches = [
      ./zsh-notifications.patch
    ] ++ lib.optionals (vte-majorminor-version == "0.60") [
      # Support nixos-20.09
      (fetchpatch {
        url = mkVtePatchUrl "820fc6f43a095a7f0c5f3052f4b0d689dbbb1aba";
        sha256 = "1hn64b3czxp2qbfh74bkfb2rilibwsm3qmww9rczlp117y07a7y4";
      })
    ] ++ lib.optionals (vte-majorminor-version == "0.62") [
      # Support nixos-21.03
      (fetchpatch {
        url = mkVtePatchUrl "7855b397b093cac06224cf728ca3962faebfe9c8";
        sha256 = "o4mIzLMIbKIIfq6hCM2aUDQ0yJDM0MpOZjKjHS++dwY=";
      })
    ];
  });

  # Tilix uses gtkd with bindings to vte. We override it's vte so it can have notifications support.
  gtkd-notifications = prev.gtkd.override { vte = final.vte-notifications; };

  # Notifications support in tilix
  tilix = prev.tilix.override { gtkd = final.gtkd-notifications; };

  # Notifications support in gnome-terminal
  gnome3 = prev.gnome3.overrideScope' (gfinal: gprev: {
    gnome-terminal = (gprev.gnome-terminal.overrideAttrs (old: {
      nativeBuildInputs = with final; [
        autoreconfHook
        yelp-tools
      ] ++ old.nativeBuildInputs;

      # https://src.fedoraproject.org/rpms/gnome-terminal/blob/master/f/gnome-terminal-cntr-ntfy-autottl-ts.patch
      patches = lib.optionals (gnome-terminal-majorminor-version == "3.36") [
        # Support nixos-20.09
        (fetchpatch {
          url = mkGnomeTerminalPatchUrl "f30999752f2f5a3dbbf4748369550b6019c167ee";
          sha256 = "xhGdfyMNKQEfJlJZJPiCg7eqpCXGQ2fLxLLTBDZv7xM=";
        })
      ] ++ lib.optionals (gnome-terminal-majorminor-version == "3.38") [
        # Support nixos-21.03
        (fetchpatch {
          url = mkGnomeTerminalPatchUrl "46de13c564de9f43c2064b28afb4400fde64f05d";
          sha256 = "N4LQY4D+bRKbYW2GoNZ2HNLmnPV22f/+s5WLwFTymPQ=";
        })
      ];

      # looks for dbus file from gnome-shell in the
      # installation tree of the package it is configuring.
      postPatch = ''
        substituteInPlace configure.ac \
          --replace '$(eval echo $(eval echo $(eval echo ''${dbusinterfacedir})))/org.gnome.ShellSearchProvider2.xml' "${final.gnome3.gnome-shell}/share/dbus-1/interfaces/org.gnome.ShellSearchProvider2.xml"
        substituteInPlace src/Makefile.am \
          --replace '$(dbusinterfacedir)/org.gnome.ShellSearchProvider2.xml' "${final.gnome3.gnome-shell}/share/dbus-1/interfaces/org.gnome.ShellSearchProvider2.xml"
      '';

      postFixup = ''
        cp ${./org.gnome.Terminal.gschema.override} $out/share/gsettings-schemas/${old.pname}-${old.version}/glib-2.0/schemas/org.gnome.Terminal.schema.override
      '';
    })).override {
      vte = final.vte-notifications;
    };

  });
}
